const Api = function () {
    this._lang = window.Laravel.lang;
    this._apiUrl = 'https://mu1.panama.kz';
    this._apiBase = `${this._apiUrl}/${this._lang}`;

    this.filesUploadUrl = this._apiBase + "/api/upload/file";
};