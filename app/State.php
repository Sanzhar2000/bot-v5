<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\State
 *
 * @property int $id
 * @property string $chat_id
 * @property string|null $state
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|State newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|State newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|State query()
 * @method static \Illuminate\Database\Eloquent\Builder|State whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|State whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|State whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|State whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|State whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $step
 * @property string|null $lang
 * @property int|null $category_id
 * @property int|null $sub_category_id
 * @property int|null $question_id
 * @method static \Illuminate\Database\Eloquent\Builder|State whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|State whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|State whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|State whereStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|State whereSubCategoryId($value)
 */
class State extends Model
{
    protected $fillable = ["chat_id"];
    //
}
