<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Spatie\Translatable\HasTranslations;

/**
 * App\SubCategory
 *
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $category_id
 * @property array $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Category $category
 * @property-read array $translations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $questions
 * @property-read int|null $questions_count
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereUpdatedAt($value)
 * @property string $title_en
 * @property string $title_ru
 * @property string $title_kk
 * @property-read mixed $title
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereTitleEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereTitleKk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubCategory whereTitleRu($value)
 */
class SubCategory extends Model
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function getTitleAttribute()
    {
        $title = 'title_' . App::getLocale();
        return $this->$title;
    }
}
