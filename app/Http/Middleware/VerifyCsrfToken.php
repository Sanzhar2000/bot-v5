<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/1433960983:AAEEi3jX5E37Y15tpOZ8WD-f_E2nXLvcGBI/webhook'
    ];


//    public function __construct(Application $app, Encrypter $encrypter)
//    {
//        parent::__construct($app, $encrypter);
//        $this->except[] = '/'.config('telegram.bot_token').'/webhook';
//    }
}
