<?php

namespace App\Http\Controllers;

use App\Category;
use App\Question;
use App\State;
use App\SubCategory;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Telegram\Bot\Api;

class MainController extends Controller
{
    protected $telegram;

    public function __construct()
    {
        $this->telegram = new Api(config('telegram.bot_token'));
    }

    public function test()
    {

        //dd(App::getLocale(), Category::where('title_' . , '=', 'Категория_:2')->first());
        Cache::clear();

        return 'ok';
    }

    public function returnBack($text, $chatId, State $state)
    {
        if ($state->step == 2) {
            $state->step = 0;
            $state->lang = null;
            $state->save();
            $this->replyWithLanguagesKeyboard($chatId, $state);
        }
        if ($state->step == 3) {
            $state->step = 1;
            $state->save();
            $this->replyWithCategoriesKeyboard($text, $chatId, $state, true);
        }
        if ($state->step == 4) {
            $state->step = 2;
            $state->sub_category_id = null;
            $state->save();
            $this->replyWithSubCategoriesKeyboard($text, $chatId, $state, true);
        }
        if ($state->step == 5) {
            $state->step = 3;
            $state->question_id = null;
            $state->save();
            $this->replyWithQuestionsKeyboard($text, $chatId, $state, true);
        }
    }

    public function replyWithLanguagesKeyboard($chatId, State $state)
    {
        $keyboard = [
            ['EN'],
            ['RU'],
            ['KK'],
        ];

        $reply_markup = $this->telegram->replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Выберите язык: ',
            'reply_markup' => $reply_markup,
        ]);

        $state->step = 1;
        $state->save();
    }

    public function replyWithCategoriesKeyboard($text, $chatId, State $state, $back = false)
    {
        if ($text == 'EN') {
            App::setLocale('en');
        }
        if ($text == 'RU') {
            App::setLocale('ru');
        }
        if ($text == 'KK') {
            App::setLocale('kk');
        }

        if ($back == true) {
            App::setLocale($state->lang);
        }

        $keyboard = Category::pluck('title_' . App::getLocale())->map(function($item, $key) {
            return [$item];
        })->toArray();

        if (App::getLocale() == 'ru') {
            array_push($keyboard, ['Назад']);
        } else if (App::getLocale() == 'kk') {
            array_push($keyboard, ['Артқа']);
        } else {
            array_push($keyboard, ['Back']);
        }

        $reply_markup = $this->telegram->replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Выберите категорию: ',
            'reply_markup' => $reply_markup,
        ]);

        $state->step = 2;
        if ($back == false) {
            $state->lang = App::getLocale();
        }
        $state->save();
    }

    public function replyWithSubCategoriesKeyboard($text, $chatId, State $state, $back = false)
    {
        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 1',
        ]);
        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => $text,
        ]);
        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 4',
        ]);

        if ($back == true) {
            $category = Category::find($state->category_id);
        } else {
            $category = Category::where('title_' . $state->lang, '=', $text)->first();
        }

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 5',
        ]);
        $categoryId = $category->id;

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 6',
        ]);

        if ($back == true) {
            $this->telegram->sendMessage([
                'chat_id' => $chatId,
                'text' => 'Sample 7: ' . Category::find($state->category_id)->title,
            ]);
        } else {
            $this->telegram->sendMessage([
                'chat_id' => $chatId,
                'text' => 'Sample 7: ' . Category::where('title_'. $state->lang, '=', $text)->first()->title,
            ]);
        }

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 8',
        ]);

        $subCategories = $category->subCategories()->pluck('title_' . $state->lang)->toArray();

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 9',
        ]);

        $keyboard = [];
        foreach ($subCategories as $subCategory) {
            $row = [];
            array_push($row, $subCategory);
            array_push($keyboard, $row);
        }
        if ($state->lang == 'ru') {
            array_push($keyboard, ['Назад']);
        } else if ($state->lang == 'kk') {
            array_push($keyboard, ['Артқа']);
        } else {
            array_push($keyboard, ['Back']);
        }

        $reply_markup = $this->telegram->replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Выберите под категорию: ',
            'reply_markup' => $reply_markup,
        ]);


        $state->step = 3;
        $state->category_id = $categoryId;
        $state->save();
    }

    private function replyWithQuestionsKeyboard($text, $chatId, State $state, $back = false)
    {
        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 10',
        ]);
        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => $text,
        ]);
        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 11',
        ]);

        $category = Category::find($state->category_id);

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 12',
        ]);
        $categoryId = $category->id;

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 13',
        ]);

        if ($back == true) {
            $subCategory = SubCategory::find($state->sub_category_id);
        } else {
            $subCategory = SubCategory::where([
                ['category_id', '=', $categoryId],
                ["title_".$state->lang, '=', $text]
            ])->first();
        }

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 14',
        ]);
        $subCategoryId = $subCategory->id;

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 15',
        ]);

        $questions = $subCategory->questions()->pluck('title_'.$state->lang)->toArray();
        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 16',
        ]);

        $keyboard = [];
        foreach ($questions as $question) {
            $row = [];
            array_push($row, $question);
            array_push($keyboard, $row);
        }
        if ($state->lang == 'ru') {
            array_push($keyboard, ['Назад']);
        } else if ($state->lang == 'kk') {
            array_push($keyboard, ['Артқа']);
        } else {
            array_push($keyboard, ['Back']);
        }

        $reply_markup = $this->telegram->replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Выберите под вопрос: ',
            'reply_markup' => $reply_markup,
        ]);

        $state->step = 4;
        //$state->category_id = $categoryId;
        $state->sub_category_id = $subCategoryId;
        $state->save();
    }

    private function replyWithAnswersKeyboard($text, $chatId, State $state)
    {

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 20',
        ]);
        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => $text,
        ]);
        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 21',
        ]);

        $question = Category::find($state->category_id)->subCategories->find($state->sub_category_id)->questions->where('title_'.$state->lang, '=', $text)->first();

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 24',
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Question ID: ' . $question->id,
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 25',
        ]);

        $keyboard = [];
        if ($state->lang == 'ru') {
            array_push($keyboard, ['Назад']);
        } else if ($state->lang == 'kk') {
            array_push($keyboard, ['Артқа']);
        } else {
            array_push($keyboard, ['Back']);
        }

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 27',
        ]);

        $reply_markup = $this->telegram->replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 27.55555',
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Ответ: ' . $question->answer,
            'reply_markup' => $reply_markup,
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 28',
        ]);

        $questionId = $question->id;

        $this->telegram->sendMessage([
            'chat_id' => $chatId,
            'text' => 'Sample 29',
        ]);

        $state->step = 5;
        //$state->category_id = $categoryId;
        //$state->sub_category_id = $subCategoryId;
        $state->question_id = $questionId;
        $state->save();

    }

    public function index()
    {
        $updates = $this->telegram->getWebhookUpdates();

        $message = $updates->getMessage();
        $user = $message->getFrom();
        $text = $message->getText();

        $this->telegram->sendMessage([
            'chat_id' => $user->getId(),
            'text' => '55555 Вот ваш id: ' . (string) $user->getId(),
        ]);

        if (!State::where('chat_id', $user->getId())->exists()){
            State::create([
                'chat_id' => $user->getId(),
                'state' => null,
            ]);
        }

        $state = State::where('chat_id', $user->getId())->first();

        $this->telegram->sendMessage([
            'chat_id' => $user->getId(),
            'text' => '77777 Вот ваш id: ' . (string) $user->getId(),
        ]);

        if (in_array($text, ['Back', 'Назад', 'Артқа'])) {
            $this->returnBack($text, $user->getId(), $state);
        } else if ($state->step == 1) {
            $this->replyWithCategoriesKeyboard($text, $user->getId(), $state);
        } else if ($state->step == 2) {
            $this->replyWithSubCategoriesKeyboard($text, $user->getId(), $state);
        } else if ($state->step == 3) {
            $this->replyWithQuestionsKeyboard($text, $user->getId(), $state);
        } else if ($state->step == 4) {
            $this->replyWithAnswersKeyboard($text, $user->getId(), $state);
        } else {
            $this->replyWithLanguagesKeyboard($user->getId(), $state);
        }
    }
}
