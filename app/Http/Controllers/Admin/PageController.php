<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageStoreRequest;
use App\Models\Page;
use App\Models\Worker;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::paginate(10);

        return view('admin.pages.page.index', compact('pages'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        $workers = $page->workers()->paginate(10);
        $allWorkers = Worker::all();

        return view('admin.pages.page.show', compact('page', 'workers', 'allWorkers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('admin.pages.page.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageStoreRequest $request, Page $page)
    {
        $validated = $request->validated();

        foreach (['image_ru', 'image_en', 'image_kk'] as $key)
            if ($image = $request->file($key)) {
                $destinationPath = 'upload/images/pages/';
                $profileImage = date('Y-m-d-H-i-s') . "_"  . $image->getClientOriginalName();
                $image->move($destinationPath, $profileImage);
                $validated[$key] = '/' . $destinationPath . $profileImage;
            }

        $page->update($validated);

        return redirect()->route('panel.pages.show', $page)->with('success','Страница обновлен успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->forceDelete();

        return redirect( route('panel.pages.index'))->with('success', 'Страница успешно удален');
    }

    public function addWorker(Page $page)
    {
        $page->workers()->sync(request('worker_ids'), false);

        return redirect()->back()->with('success', 'Сотрудник дабавлен успешно');
    }

    public function deleteWorker(Page $page, Worker $worker)
    {
        $page->workers()->detach($worker);

        return redirect()->back()->with('success', 'Сотрудник удален успешно');
    }
}
