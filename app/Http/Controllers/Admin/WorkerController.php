<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\WorkerStoreRequest;
use App\Models\Page;
use App\Models\Worker;
use Illuminate\Http\Request;

class WorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workers = Worker::paginate(10);

        return view('admin.pages.worker.index', compact('workers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.worker.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkerStoreRequest $request)
    {
        $validated = $request->validated();

        if ($image = $request->file('image')) {
            $destinationPath = 'upload/images/workers/';
            $profileImage = date('Y-m-d-H-i-s') . "_"  . $image->getClientOriginalName();
            $image->move($destinationPath, $profileImage);
            $validated['image'] = '/' . $destinationPath . $profileImage;
        }

        $worker = Worker::create($validated);

        return redirect()->route('panel.workers.show', $worker->id)->with('success','Сотрудник создан успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Worker $worker)
    {
        return view('admin.pages.worker.show', compact('worker'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Worker $worker)
    {
        return view('admin.pages.worker.edit', compact('worker'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WorkerStoreRequest $request, Worker $worker)
    {
        $validated = $request->validated();

        if ($image = $request->file('image')) {
            $destinationPath = 'upload/images/workers/';
            $profileImage = date('Y-m-d-H-i-s') . "_"  . $image->getClientOriginalName();
            $image->move($destinationPath, $profileImage);
            $validated['image'] = '/' . $destinationPath . $profileImage;
        }

        $worker->update($validated);

        return redirect()->route('panel.workers.show', $worker->id)->with('success','Сотрудник обновлен успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Worker $worker)
    {
        $worker->forceDelete();

        return redirect( route('panel.workers.index'))->with('success', 'Сотрудник успешно удален');
    }
}
