<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticlesStoreRequest;
use App\Models\Article;
use App\Models\Dictionary;
use Illuminate\Support\Str;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::paginate(10);

        return view('admin.pages.article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Dictionary::where('alias', 'article_category')->first()->items;

        return view('admin.pages.article.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticlesStoreRequest $request)
    {
        $validated = $request->validated();

        $validated['alias'] = Str::slug($validated['title_ru']);

        if ( $request->is_published == '1' && !$request->published_at) {
            $validated['published_at'] = now();
        }

        if ($image = $request->file('image')) {
            $destinationPath = 'upload/images/news/';
            $profileImage = date('Y-m-d-H-i-s') . "_"  . $image->getClientOriginalName();
            $image->move($destinationPath, $profileImage);
            $validated['image'] = '/' . $destinationPath . $profileImage;
        }

        $article = Article::create($validated);

        return redirect()->route('panel.articles.show', $article)->with('success','Новость создан успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('admin.pages.article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $categories = Dictionary::where('alias', 'article_category')->first()->items;

        return view('admin.pages.article.edit', compact('article', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticlesStoreRequest $request, Article $article)
    {
        $validated = $request->validated();

        $validated['alias'] = Str::slug($validated['title_ru']);

        if ( $request->is_published == '1' && !$request->published_at) {
            $validated['published_at'] = now();
        }

        if ($image = $request->file('image')) {
            $destinationPath = 'upload/images/news/';
            $profileImage = date('Y-m-d-H-i-s') . "_"  . $image->getClientOriginalName();
            $image->move($destinationPath, $profileImage);
            $validated['image'] = '/' . $destinationPath . $profileImage;
        }

        $article->update($validated);

        return redirect(route('panel.articles.show', $article))->with('success','Новость обновлен успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->forceDelete();

        return redirect( route('panel.articles.index'))->with('success', 'Новость успешно удален');
    }

//    public function publish(Article $article)
//    {
//        $article->update([
//           'is_published' => $article->is_published == 0 ? 1 : 0,
//           'published_at' => $article->is_published == 0 ? now() : null,
//        ]);
//
//        return redirect()->back()->with('success','Новость обновлен успешно');
//    }
}
