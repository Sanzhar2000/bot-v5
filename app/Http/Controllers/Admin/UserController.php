<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->type == 'registered')
            $users = User::where('union_member', false)->paginate(10);

        elseif ($request->type == 'members')
            $users = User::where('union_member', true)->paginate(10);

        else
            $users = User::paginate(10);

        return view('admin.pages.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();

        return view('admin.pages.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));


        return redirect()->route('panel.users.show', $user)->with('success','Пользователь создан успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.pages.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.pages.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ])->after(function ($validator) {
            return redirect()->back()->withErrors($validator, 'updateProfile');
        });

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return redirect()->route('panel.users.show', $user)->with('success','Пользователь обновлен успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->forceDelete();

        return redirect( route('panel.users.index'))->with('success', 'Пользователь успешно удален');
    }

    public function updatePassword(Request $request){
        $user = auth()->user();

        Validator::make($request->all(), [
            'password' => 'required|different:old_password|min:8',
            'password_confirmation' => 'required|same:password'
        ])->after(function ($validator) {
            return redirect()->back()->withErrors($validator, 'updatePassword');
        });

        $user->update([
            'password' => bcrypt($request->password)
        ]);

        return redirect()->route('panel.users.show', $user)->with('success', 'Пароль успешно изменен');
    }
}
