<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function show(){
        $user = auth()->user();

        return view('admin.pages.profile.show', compact('user'));
    }

    public function edit(){
        $user = auth()->user();

        return view('admin.pages.profile.edit', compact('user'));
    }

    public function update(Request $request){
         Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ])->after(function ($validator) {
            return redirect()->back()->withErrors($validator, 'updateProfile');
        });

        auth()->user()->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return redirect()->route('panel.profile.show', [\App::currentLocale()])->with('success', 'Данные успешно изменены');
    }

    public function updatePassword(Request $request){
        $user = auth()->user();

        if (\request('old_password') && Hash::check($request->old_password, $user->password)){
            Validator::make($request->all(), [
                'old_password' => 'required',
                'new_password' => 'required|different:old_password|min:8',
                'new_password_confirmation' => 'required|same:password'
            ])->after(function ($validator) {
                return redirect()->back()->withErrors($validator, 'updatePassword');
            });

            $user->update([
                'password' => $request->new_password
            ]);

            return redirect()->route('panel.profile.show', [\App::currentLocale()])->with('success', 'Данные успешно изменены');
        }

        return redirect()->back()->with('updatePassword', 'Пароль введен неправильно');
    }
}
