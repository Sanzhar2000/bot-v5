<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DictionaryStoreRequest;
use App\Models\Dictionary;
use Illuminate\Http\Request;

class DictionaryController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Dictionary $dictionary)
    {
        $items = $dictionary->items()->paginate(10);

        return view('admin.pages.dictionary.show', compact('dictionary', 'items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Dictionary $dictionary)
    {
        return view('admin.pages.dictionary.edit', compact('dictionary'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DictionaryStoreRequest $request, Dictionary $dictionary)
    {
        $dictionary->update($request->validated());

        return redirect()->route('panel.dictionaries.show', $dictionary)->with('success','Справочник обновлен успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dictionary $dictionary)
    {
        $dictionary->forceDelete();

        return redirect( route('panel.dictionaries.index'))->with('success', 'Справочник успешно удален');
    }
}
