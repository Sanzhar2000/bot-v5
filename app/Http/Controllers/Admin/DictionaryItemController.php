<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DictionaryItemStoreRequest;
use App\Http\Requests\DictionaryStoreRequest;
use App\Models\Dictionary;
use App\Models\DictionaryItem;
use Illuminate\Http\Request;

class DictionaryItemController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Dictionary $dictionary)
    {
        return view('admin.pages.dictionary.item.create', compact('dictionary'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DictionaryItemStoreRequest $request, Dictionary $dictionary)
    {
        $validated = $request->validated();

        $item = $dictionary->items()->create($validated);

        return redirect()->route('panel.dictionary-items.show', [$dictionary->id, $item->id])->with('success','Справочник создан успешно');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Dictionary $dictionary, DictionaryItem $item)
    {
        return view('admin.pages.dictionary.item.show', compact('dictionary', 'item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Dictionary $dictionary, DictionaryItem $item)
    {
        return view('admin.pages.dictionary.item.edit', compact('dictionary', 'item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DictionaryItemStoreRequest $request, Dictionary $dictionary, DictionaryItem $item)
    {
        $item->update($request->validated());

        return redirect()->route('panel.dictionary-items.show', [$dictionary->id, $item->id])->with('success','Справочник обновлен успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dictionary $dictionary, DictionaryItem $item)
    {
        $item->forceDelete();

        return redirect( route('panel.dictionaries.show', $dictionary->id))->with('success', 'Справочник успешно удален');
    }
}
