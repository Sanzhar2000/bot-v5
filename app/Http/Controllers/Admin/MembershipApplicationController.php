<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MembershipApplicationStoreRequest;
use App\Models\MembershipApplication;
use App\Models\User;
use Illuminate\Http\Request;

class MembershipApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $applications = MembershipApplication::paginate(10);
        $applications = MembershipApplication::where('accepted', false)->paginate(10);

        return view('admin.pages.application.index', compact('applications'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MembershipApplication $application)
    {
        return view('admin.pages.application.show', compact('application'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MembershipApplication $application)
    {
        return view('admin.pages.application.edit', compact('application'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MembershipApplicationStoreRequest $request, MembershipApplication $application)
    {
        $application->update($request->validated());

        return redirect()->route('panel.applications.show', $application)->with('success','Заявка обновлен успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MembershipApplication $application)
    {
        $application->forceDelete();

        return redirect( route('panel.applications.index'))->with('success', 'Заявка успешно удален');
    }

    public function accept(MembershipApplication $application)
    {
        $application->update(['accepted' => 1]);

        if ($user = User::where('email', $application->email)->first()) {
            $user->update(['union_member' => true]);
        }
        else {
            $user = User::create([
                'name' => $application->name,
                'email' => $application->email,
                'phone' => $application->phone,
                'union_member' => true,
            ]);
        }

        $user->assignRole('member_msk');

        return redirect( route('panel.applications.index'))->with('success', 'Заявка успешно одобрен');
    }
}
