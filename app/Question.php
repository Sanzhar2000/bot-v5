<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Spatie\Translatable\HasTranslations;

/**
 * App\Question
 *
 * @property int $id
 * @property int $sub_category_id
 * @property array $title
 * @property array $answer
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read array $translations
 * @property-read \App\SubCategory $subCategory
 * @method static \Illuminate\Database\Eloquent\Builder|Question newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Question newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Question query()
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereSubCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $title_en
 * @property string $title_ru
 * @property string $title_kk
 * @property string $answer_en
 * @property string $answer_ru
 * @property string $answer_kk
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereAnswerEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereAnswerKk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereAnswerRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereTitleEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereTitleKk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereTitleRu($value)
 */
class Question extends Model
{
    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class);
    }

    public function getTitleAttribute()
    {
        $title = 'title_' . App::getLocale();
        return $this->$title;
    }

    public function getAnswerAttribute()
    {
        $answer = 'answer_' . App::getLocale();
        return $this->$answer;
    }
}
