<?php

use App\Http\Controllers\Auth\LoginController as PanelLoginController;

use Illuminate\Support\Facades\Route;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Telegram routes
Route::post('/' . config('telegram.bot_token') . '/webhook', [MainController::class, 'index']);

Route::get('/test', [MainController::class, 'test']);

Route::get('/set/webhook', function() {
    $response = Telegram::setWebhook([
        'url' => env('APP_URL') . config('telegram.bot_token') . '/webhook'
    ]);

    return $response;
});

Route::get('/remove/webhook', function (){
    $response = Telegram::removeWebhook();

    return $response;
});

Route::redirect('/panel', '/panel/profile/show')->name('panel.main.url');

/** Admin Panel */
Route::get('panel/login', [PanelLoginController::class, 'showPanelLoginForm'])->name('panel.login');

Route::prefix('panel')->middleware(['auth', 'role:admin|manager'])->name('panel.')->group(function ($router) {
    /** Profile */
    $router->prefix('profile')->name('profile.')->group(function ($router) {
        $router->get('show', [ProfileController::class, 'show'])->name('show');
        $router->get('edit', [ProfileController::class, 'edit'])->name('edit');
        $router->put('edit', [ProfileController::class, 'update'])->name('update');
        $router->put('updatePassword', [ProfileController::class, 'updatePassword'])->name('updatePassword');
    });

    /** Users */
    $router->resource('users', UserController::class);
    $router->put('users/{user}/updatePassword', [UserController::class, 'updatePassword'])->name('users.update_password');

    /** Articles */
    $router->resource('articles', ArticlesController::class);

    /** Pages */
    $router->resource('pages', PageController::class, ['except' => ['store', 'create']]);
    $router->post('pages/{page}/worker/add', [PageController::class, 'addWorker'])->name('pages.worker.add');
    $router->post('pages/{page}/worker/{worker}/delete', [PageController::class, 'deleteWorker'])->name('pages.worker.delete');

    /** Workers */
    $router->resource('workers', WorkerController::class);

    /** Orders */
    $router->resource('orders', OrderController::class);

    /** Roles */
    $router->resource('roles', RoleController::class);

    /** Dictionaries */
    $router->resource('dictionaries', DictionaryController::class);

    /** Dictionary Items */
    $router->resource('dictionaries/{dictionary}/items', DictionaryItemController::class, ['names' => 'dictionary-items']);

    /** Membership Applications */
    $router->resource('applications', MembershipApplicationController::class, ['except' => ['store', 'create']]);
    $router->put('applications/{application}/accept', [MembershipApplicationController::class, 'accept'])->name('application.accept');
});

/** General routes */
Route::prefix('')->name('general.')->group(function ($router) {
    $router->get('/', [App\Http\Controllers\General\PageController::class, 'index'])->name('index');
    $router->get('/page/{alias}', [App\Http\Controllers\General\PageController::class, 'view'])->name('page.view');

    $router->get('/analytics', [MainController::class, 'analytics'])->name('analytics');
    $router->get('/analytics-export', [ExportController::class, 'analytics'])->name('analytics-export');

    $router->get('/analytics-sale', [MainController::class, 'analyticsSale'])->name('analytics-sale');
    $router->get('/analytics-sale-export', [ExportController::class, 'analyticsSale'])->name('analytics-sale-export');

    $router->get('/analytics-import', [MainController::class, 'analyticsImport'])->name('analytics-import');
    $router->get('/analytics-import-export', [ExportController::class, 'analyticsImport'])->name('analytics-import-export');

    $router->get('/articles', [MainController::class, 'articles'])->name('articles');
    $router->get('/article/{article}', [MainController::class, 'article'])->name('article.show');
    $router->get('/contact', [App\Http\Controllers\General\PageController::class, 'contact'])->name('contact');

    $router->middleware('auth')->group(function ($router) {
        $router->get('/profile', [MainController::class, 'profile'])->name('profile');
        $router->post('/profile/update', [App\Http\Controllers\General\UserController::class, 'update'])->name('profile-update');
        $router->post('/profile/update-password', [App\Http\Controllers\General\UserController::class, 'updatePassword'])->name('profile-update-password');
    });

    $router->post('/publish-ad', [MainController::class, 'publishAd'])->name('publish-ad');

    /** Auth routes */
    $router->get('/login', [PanelLoginController::class, 'showLoginForm'])->name('show_login');
    $router->post('/login', [PanelLoginController::class, 'login'])->name('login');

    $router->get('/password-recovery', [AuthController::class, 'showPasswordRecoveryForm'])->name('password-recovery');

    $router->get('/registration', [RegisterController::class, 'showRegistrationForm'])->name('registration');
    $router->post('/register', [RegisterController::class, 'register'])->name('register');

    $router->get('/registration-member', [AuthController::class, 'showRegistrationMemberForm'])->name('registration-member');
    $router->post('/register-member', [AuthController::class, 'registerMember'])->name('register-member');

    $router->post('/logout', [LoginController::class, 'logout'])->middleware('auth')->name('logout');
    /** Auth routes */
});

