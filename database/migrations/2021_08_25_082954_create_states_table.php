<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->id();

            $table->string('chat_id');
            $table->smallInteger('step')->default(null)->nullable();
            $table->string('lang')->default(null)->nullable();
            $table->integer('category_id')->default(null)->nullable();
            $table->integer('sub_category_id')->default(null)->nullable();
            $table->integer('question_id')->default(null)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
