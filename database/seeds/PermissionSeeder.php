<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',
            'article-list',
            'article-create',
            'article-edit',
            'article-delete',
            'page-list',
            'page-create',
            'page-edit',
            'page-delete',
            'worker-list',
            'worker-create',
            'worker-edit',
            'worker-delete',
            'order-list',
            'order-create',
            'order-edit',
            'order-delete',
            'application-list',
            'application-create',
            'application-edit',
            'application-delete',
            'dictionary-list',
            'dictionary-create',
            'dictionary-edit',
            'dictionary-delete',
            'dictionary-item-list',
            'dictionary-item-create',
            'dictionary-item-edit',
            'dictionary-item-delete',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
