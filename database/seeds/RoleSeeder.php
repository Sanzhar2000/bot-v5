<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'admin',
            'manager',
            'plemreproductor',
            'otkormploschadka',
            'meat_processing_complex',
            'farmer_member_msk',
            'farmer_not_member_msk',
            'unauthorized_user'
        ];

        foreach($roles as $role) {
            Role::create(['name' => $role]);
        }

        $role_admin = Role::where('name', 'admin')->first();
        $role_admin->syncPermissions(Permission::pluck('id', 'id')->all());

        $manager_permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'permission-list',
            'permission-create',
            'permission-edit',
            'user-list',
            'user-create',
            'user-edit',
            'article-list',
            'article-create',
            'article-edit',
        ];

        $role_manager = Role::where('name', 'manager')->first();
        $role_manager->syncPermissions($manager_permissions);
    }
}
