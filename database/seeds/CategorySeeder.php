<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 4; $i++){
            Category::create([
                'title_en' => "Category_" . $i,
                'title_ru' => "Категория_:" . $i,
                'title_kk' => "Қатегөрия_" . $i,
            ]);
        }
    }
}
