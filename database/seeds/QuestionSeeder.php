<?php

use App\Question;
use App\SubCategory;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 16; $i++) {
            $subCategory = SubCategory::find($i);

            for ($j = 1; $j <= 5; $j++) {
                $question = new Question();
                $question->title_en = 'Question: ' . $j;
                $question->title_ru =  'Вопрос: ' . $j;
                $question->title_kk =  'Сұрақ: ' . $j;

                $question->answer_en = 'Answer: ' . $j;
                $question->answer_ru =  'Ответ: ' . $j;
                $question->answer_kk =  'Жауап: ' . $j;

                $subCategory->questions()->save($question);
            }
        }
    }
}
