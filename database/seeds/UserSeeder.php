<?php

use App\User;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    public $faker;

    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        $user_admin = User::create([
            'name' => 'User Admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123456789'),
        ]);

        $user_admin->assignRole(Role::where('name', 'admin')->first());

        // Manager
        $user_manager = User::create([
            'name' => 'User Manager',
            'email' => 'manager@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('123456789')
        ]);

        $user_manager->assignRole(Role::where('name', 'manager')->first());

        // Fake users
        foreach (range(1, 10) as $i) {
            User::create([
                'name' => $this->faker->name,
                'password' => bcrypt('password'),
                'email' => $this->faker->email,
            ]);
        }

        // User
        User::create([
            'name' => 'User',
            'email' => 'user@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('12345678')
        ]);

        //
    }
}
