<?php

use App\Question;
use App\SubCategory;
use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 4; $i++){
            for ($j = 1; $j <= 4; $j++){
                SubCategory::create([
                    'category_id' => $i,
                    'title_en' => "SubCategory" . $i . " : " . $j,
                    'title_ru' =>  "СубКатегория" . $i . " : " . $j,
                    'title_kk' =>  "СубҚатегөрия" . $i . " : " . $j,
                ]);
            }
        }
    }
}
