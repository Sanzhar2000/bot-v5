@extends('admin.layouts.index')

@section('title', 'Пользователи')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <h1 class="title-primary" style="margin-bottom: 0">Управление пользователями</h1>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach($errors as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif

    <div class="block">
    <div class="tabs">
        <div class="mobile-dropdown">
            <div class="mobile-dropdown__title dynamic">Пользователи</div>
            <div class="mobile-dropdown__desc">
                <ul class="tabs-titles">
                    <li class="active"><a href="javascript:;" title="Пользователи">Пользователи</a></li>
                </ul>
            </div>
        </div>

        <div class="tabs-contents">
            <div class="active">
                <table class="table table-bordered">
                    <colgroup>
                        <col span="1" style="width: 3%;">
                        <col span="1" style="width: 25%;">
                        <col span="1" style="width: 20%;">
                        <col span="1" style="width: 25%;">
                    </colgroup>
                    <tr>
                        <th>ID</th>
                        <th>ФИО</th>
                        <th>Почта</th>
                        <th width="380px">Действия</th>
                    </tr>
                    @foreach ($users as $key => $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                <a class="icon-btn icon-btn--green icon-eye" href="{{ route('panel.users.show', $user->id) }}" title="Смотреть"></a>
                                @can('order-edit')
                                    <a class="icon-btn icon-btn--yellow icon-edit" href="{{ route('panel.users.edit', $user->id) }}" title="Редактировать"></a>
                                @endcan
                                @can('order-delete')
                                    <a href="javascript:;" title="Удалить"
                                       onclick="document.querySelector('#model-{{ $user->id }}').submit()"
                                       class="icon-btn icon-btn--pink icon-delete"></a>
                                    <form action="{{ route('panel.users.destroy',  $user->id) }}" id="model-{{ $user->id }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </table>

                {{ $users->appends(\Illuminate\Support\Facades\Request::except('page'))->links("vendor.pagination.admin") }}
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
        <!---->
@endsection
