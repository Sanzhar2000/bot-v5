@extends('admin.layouts.index')

@section('title', 'Пользователи')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.users.index') }}">Пользователи</a></li>
                    <li><span>Новый</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.users.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="block" method="post" action="{{route('panel.users.store') }}">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="input-group">
                    <div class="input-group">
                        <label class="input-group__title"> ФИО</label>
                        <input type="text" name="name" value="" placeholder="Name" class="input-regular" required>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="input-group">
                    <div class="input-group">
                        <label class="input-group__title">Почта</label>
                        <input type="email" name="email" value="" placeholder="Email" class="input-regular" required>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="input-group">
                    <div class="input-group">
                        <label class="input-group__title">Пароль</label>
                        <input type="password" name="password" class="input-regular" placeholder="Password" data-validate="password">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="input-group">
                    <div class="input-group">
                        <label class="input-group__title">Подтвердите пароль</label>
                        <input type="password" name="confirm-password" class="input-regular" placeholder="Confirm Password" data-validate="password">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="input-group">
                    <div class="input-group">
                        <label class="input-group__title">Роль</label>
                        <select class="input-regular" name="roles">
                            @foreach($roles as $role)
                                <option value="{{ $role }}">{{$role}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </form>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
