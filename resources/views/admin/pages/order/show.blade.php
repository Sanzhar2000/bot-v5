@extends('admin.layouts.index')

@section('title', 'Объявлении')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.orders.index') }}">Управление заявками</a></li>
                    <li><span>#{{ $order->id }}</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.orders.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="block">
        <h2 class="title-primary">Заявка</h2>
        <div class="row">
            <div class="col-md-8 text-left-md text-left-lg">
                <div class="input-group">
                    <div class="input-group__title">Владелец</div>
                    <input type="text" value="{{ $order->user->name }}" placeholder="Владелец" class="input-regular" name="user_id" disabled>
                </div>
                <div class="input-group">
                    <div class="input-group__title">Описание</div>
                    <textarea name="description" class="tinymce-text-here input-regular" disabled>{{ $order->description }}</textarea>
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group__title">Фотограция</div>
                    <a href="{{ $order->image }}" target="_blank">
                        <img class="article-image" src="{{$order->image ?? ''}}" width="380" height="250">
                    </a>
                </div>
            </div>
        </div>
        <div class="input-group">
            <div class="input-group__title">Категория</div>
            <input type="text" placeholder="Категория" class="input-regular" name="category" disabled
                   value="@if( $order->category == '1') Куплю @elseif( $order->category == '2') Продам @else Аренда @endif">
        </div>
        <div class="input-group">
            <div class="input-group__title">Регион</div>
            <input type="text" value="{{ $order->region->name }}" placeholder="Регион" class="input-regular" name="region_id" disabled>
        </div>
        <div class="input-group">
            <div class="input-group__title">Геолокация</div>
            <input type="text" value="{{ $order->location }}" placeholder="Геолокация" class="input-regular" name="location" disabled>
        </div>
        <div class="input-group">
            <label class="input-group__title">Статус</label>
            <input type="text" value="{{ $order->status }}" placeholder="Статус" class="input-regular" name="status" disabled>
        </div>
        <div class="input-group">
            <label class="input-group__title">Количество просмотров</label>
            <input type="text" value="{{ $order->views }}" placeholder="Количество просмотров" class="input-regular" name="views" disabled>
        </div>
        <div class="input-group">
            <div class="input-group__title">Количество голов</div>
            <input type="text" value="{{ $order->count }}" placeholder="Количество голов" class="input-regular" name="amount" disabled>
        </div>
        <div class="input-group">
            <div class="input-group__title">Стоимость за голову</div>
            <input type="text" value="{{ $order->price }}" placeholder="Стоимость за партию" class="input-regular" name="price" disabled>
        </div>
        <div class="input-group">
            <div class="input-group__title">Стоимость за партию</div>
            <input type="text" value="{{ $order->total_price }}" placeholder="Стоимость за партию" class="input-regular" name="total_price" disabled>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                @can('order-edit')
                    <a class="btn btn-primary" href="{{ route('panel.orders.edit', $order->id) }}">Редактировать</a>
                @endcan
                @can('order-delete')
                    <a class="btn btn-danger btn--red" href="{{ route('panel.orders.destroy', $order->id) }}"
                       onclick="event.preventDefault();
                                                         document.getElementById('delete-form').submit();" title="Удалить">Удалить
                    </a>
                    <form id="delete-form" action="{{ route('panel.orders.destroy', $order->id) }}" method="POST" class="d-none">
                        @csrf
                        @method('DELETE')
                    </form>
                @endcan
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
