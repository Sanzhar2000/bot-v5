@extends('admin.layouts.index')

@section('title', 'Объявлении')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <h1 class="title-primary" style="margin-bottom: 0">Управление заявками</h1>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
{{--                        @can('order-create')--}}
{{--                            <a class="btn btn-success" href="{{ route('panel.articles.create') }}">Добавить новость</a>--}}
{{--                        @endcan--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <br>
    <div class="block">
        <h2 class="title-secondary">Список страниц</h2>
        <table class="table table-records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 20%;">
                <col span="1" style="width: 13%;">
                <col span="1" style="width: 13%;">
                <col span="1" style="width: 7%;">
                <col span="1" style="width: 7%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 7%;">
                <col span="1" style="width: 25%;">
            </colgroup>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Описание</th>
                    <th>Пользователь</th>
                    <th>Регион</th>
                    <th>Тип</th>
                    <th>Колицество</th>
                    <th>Стоимость</th>
                    <th>Общая стоимость</th>
                    <th>Статус</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->description ?? '-' }}</td>
                        <td>{{ $order->user->name ?? '-' }}</td>
                        <td>{{ $order->region->name ?? '-' }}</td>
                        <td>
                            @if( $order->category == '1')
                                Куплю
                            @elseif( $order->category == '2')
                                Продам
                            @else
                                Аренда
                            @endif
                        </td>
                        <td>{{ $order->count ?? '-' }}</td>
                        <td>{{ $order->price ?? '-' }}</td>
                        <td>{{ $order->total_price ?? '-' }}</td>
                        <td>{{ $order->status ?? '-' }}</td>
                        <td>
                            <a class="icon-btn icon-btn--green icon-eye" href="{{ route('panel.orders.show', $order->id) }}" title="Смотреть"></a>
                            @can('order-edit')
                                <a class="icon-btn icon-btn--yellow icon-edit" href="{{ route('panel.orders.edit', $order->id) }}" title="Редактировать"></a>
                            @endcan
                            @can('order-delete')
                                <a href="javascript:;" title="Удалить"
                                   onclick="document.querySelector('#model-{{ $order->id }}').submit()"
                                   class="icon-btn icon-btn--pink icon-delete"></a>
                                <form action="{{ route('panel.orders.destroy',  $order->id) }}" id="model-{{ $order->id }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $orders->appends(\Illuminate\Support\Facades\Request::except('page'))->links("vendor.pagination.admin") }}
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
