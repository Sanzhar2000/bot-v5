@extends('admin.layouts.index')

@section('title', 'Объявлении')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.orders.index') }}">Управление заявками</a></li>
                    <li><span>Создать заявку</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.orders.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block">
        <form method="post" action="{{ route('panel.orders.store') }}">
            @csrf
            <h2 class="title-primary">Заявка</h2>
            <div class="input-group">
                <div class="input-group__title">Описание</div>
                <textarea name="description" class="tinymce-text-here input-regular"></textarea>
            </div>
            <div class="input-group">
                <label class="input-group__title">Тип</label>
                <select name="category" class="input-regular chosen" data-placeholder="" required>
                    <option value="1">Куплю</option>
                    <option value="2">Продам</option>
                    <option value="3">Аренда</option>
                </select>
            </div>
            <div class="input-group">
                <div class="input-group__title">Регион</div>
                <select name="region_id" class="input-regular chosen" data-placeholder="" required>
                    @foreach($regions as $region)
                        <option value="{{$region->id}}">{{ $region->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-group">
                <div class="input-group__title">Геолокация</div>
                <input type="text" value="" placeholder="Геолокация" class="input-regular" name="location" required>
            </div>
            <div class="input-group">
                <label class="input-group__title">Статус</label>
                <input type="number" min="1" max="5" value="" placeholder="Статус" class="input-regular" name="status" required>
            </div>
            <div class="input-group">
                <div class="input-group__title">Количество голов</div>
                <input type="number" min="1" value="" name="count" placeholder="Количество голов" class="input-regular" name="amount" required>
            </div>
            <div class="input-group">
                <div class="input-group__title">Стоимость за голову</div>
                <input type="number" min="1" value="" placeholder="Стоимость за партию" class="input-regular" name="price" required>
            </div>
            <div class="input-group">
                <div class="input-group__title">Стоимость за партию</div>
                <input type="number" min="1" value="" placeholder="Стоимость за партию" class="input-regular" name="total_price" required>
            </div>
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
