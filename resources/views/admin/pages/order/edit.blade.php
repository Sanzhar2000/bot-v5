@extends('admin.layouts.index')

@section('title', 'Объявлении')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.orders.index') }}">Управление заявками</a></li>
                    <li><a href="{{ route('panel.orders.show', $order) }}">#{{ $order->id }}</a></li>
                    <li><span>Редактировать</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.orders.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block">
        <form method="post" action="{{route('panel.orders.update', $order) }}">
            @csrf
            @method('put')
            <h2 class="title-primary">Заявка</h2>
            <div class="input-group">
                <div class="input-group__title">Описание</div>
                <textarea name="description" class="tinymce-here input-regular">{{ $order->description }}</textarea>
            </div>
            <div class="input-group">
                <label class="input-group__title">Категория</label>
                <select name="category" class="input-regular chosen" data-placeholder="" required>
                    <option value="1" {{ $order->category == '1' ? 'selected' : '' }}>Куплю</option>
                    <option value="2" {{ $order->category == '2' ? 'selected' : '' }}>Продам</option>
                    <option value="3" {{ $order->category == '3' ? 'selected' : '' }}>Аренда</option>
                </select>
            </div>
            <div class="input-group">
                <div class="input-group__title">Регион</div>
                <select name="region_id" class="input-regular chosen" data-placeholder="" required>
                    @foreach($regions as $region)
                        <option value="{{$region->id}}" {{ $order->region_id == $region->id ? 'selected' : '' }}>{{ $region->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-group">
                <div class="input-group__title">Геолокация</div>
                <input type="text" value="{{ $order->location }}" placeholder="Геолокация" class="input-regular" name="location">
            </div>
            <div class="input-group">
                <label class="input-group__title">Статус</label>
                <input type="number" min="1" max="5" value="{{ $order->status }}" placeholder="Статус" class="input-regular" name="status">
            </div>
            <div class="input-group">
                <div class="input-group__title">Количество голов</div>
                <input type="number" min="1" value="{{ $order->count }}" placeholder="Количество голов" class="input-regular" name="amount">
            </div>
            <div class="input-group">
                <div class="input-group__title">Стоимость за голову</div>
                <input type="number" min="1" value="{{ $order->price }}" placeholder="Стоимость за партию" class="input-regular" name="price">
            </div>
            <div class="input-group">
                <div class="input-group__title">Стоимость за партию</div>
                <input type="number" min="1" value="{{ $order->total_price }}" placeholder="Стоимость за партию" class="input-regular" name="total_price">
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn--green">Сохранить</button>
                    @can('order-delete')
                        <a class="btn btn-danger btn--red" href="{{ route('panel.orders.destroy', $order->id) }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();" title="Удалить">Удалить
                        </a>
                    @endcan
                </div>
            </div>
        </form>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            @can('order-delete')
                <form id="delete-form" action="{{ route('panel.orders.destroy', $order->id) }}" method="POST" class="d-none">
                    @csrf
                    @method('DELETE')
                </form>
            @endcan
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
        <script src="https://cdn.tiny.cloud/1/eutvjr9zyhc4qtchwyfwhutknw2iunsf80kiuye2fdomu2wd/tinymce/5/tinymce.min.js"
                referrerpolicy="origin"></script>
        <script src="/admin/js/redactor.js"></script>
    <!---->
@endsection
