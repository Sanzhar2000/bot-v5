@extends('admin.layouts.index')

@section('title', 'Заявки на вступление в члены МСК')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-4">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.applications.index') }}">Управление заявками на вступление в члены МСК</a></li>
                    <li><span>#{{ $application->id }}</span></li>
                </ul>
            </div>
            <div class="col-md-8 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.applications.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="block">
        <h2 class="title-primary">Заявка</h2>
        <div class="input-group">
            <div class="input-group__title">Названии организации</div>
            <input type="text" value="{{ $application->organization }}"  name="organization"placeholder="Названии организации" class="input-regular" disabled>
        </div>
        <div class="input-group">
            <div class="input-group__title">ФИО контактного лица</div>
            <input type="text" value="{{ $application->name }}" placeholder="ФИО контактного лица" class="input-regular" name="name" disabled>
        </div>
        <div class="input-group">
            <label class="input-group__title">Электронный адрес</label>
            <input type="text" value="{{ $application->email }}" placeholder="Электронный адрес" class="input-regular" name="email" disabled>
        </div>
        <div class="input-group">
            <div class="input-group__title">Номер телефона</div>
            <input type="text" value="{{ $application->phone }}" placeholder="Номер телефона" class="input-regular" name="phone" disabled>
        </div>
        <div class="input-group">
            <div class="input-group__title">Адрес</div>
            <input type="text" value="{{ $application->address }}" placeholder="Адрес" class="input-regular" name="address" disabled>
        </div>
        <div class="input-group">
            <label class="input-group__title">Площадь с/х угодий</label>
            <input type="text" value="{{ $application->area }}" placeholder="Площадь с/х угодий" class="input-regular" name="area" disabled>
        </div>
        <div class="input-group">
            <label class="input-group__title">Количество животных</label>
            <input type="text" value="{{ $application->animals_count }}" placeholder="Количество животных" class="input-regular" name="animals_count" disabled>
        </div>
        <div class="input-group">
            <div class="input-group__title">Количество голов</div>
            <input type="text" value="{{ $application->accepted }}" placeholder="Количество голов" class="input-regular" name="accepted" disabled>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                @can('application-edit')
                    <a class="btn btn-primary" href="{{ route('panel.applications.edit', $application->id) }}">Редактировать</a>
                @endcan
                @can('application-delete')
                    <a href="javascript:;" title="Одобрить"
                       onclick="document.querySelector('#model-accept-{{ $application->id }}').submit()"
                       class="btn btn-">Одобрить</a>
                    <a class="btn btn-danger btn--red" href="{{ route('panel.applications.destroy', $application->id) }}"
                       onclick="event.preventDefault();
                                                         document.getElementById('delete-form').submit();" title="Удалить">Удалить
                    </a>
                    <form id="delete-form" action="{{ route('panel.applications.destroy', $application->id) }}" method="POST" class="d-none">
                        @csrf
                        @method('DELETE')
                    </form>
                    <form action="{{ route('panel.application.accept', $application->id) }}" id="model-accept-{{ $application->id }}" method="post">
                        @csrf
                        @method('put')
                    </form>
                @endcan
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
