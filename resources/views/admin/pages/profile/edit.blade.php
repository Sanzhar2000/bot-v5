@extends('admin.layouts.index')

@section('title', 'Профиль')

@section('content')

<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><a href="{{route('panel.profile.show') }}" title="Профиль">Профиль</a></li>
        <li><span>{{$user->name}}</span></li>
    </ul>

    <form class="block" method="post" action="{{route('panel.profile.update') }}">
        @csrf
        @method('put')

        @if($errors->hasBag('updateProfile'))
            <div class="alert alert-danger">
                @foreach($errors->login as $error)
                    {{ $error }}<br>
                @endforeach
            </div>
        @endif

        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основная информация">Основная информация</a></li>
                    </ul>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title"> ФИО</label>
                        <input type="text" name="name" value="{{$user->name}}" placeholder="ФИО" class="input-regular"
                               data-required-text=">Пожалуйста, введите ФИО" required>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Почта</label>
                        <input type="email" name="email" value="{{$user->email}}" placeholder="Почта" class="input-regular"
                               data-required-text=">Пожалуйста, введите адрес электронной почты" required>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
        </div>
    </form>

    <form class="block" method="post" action="{{route('panel.profile.updatePassword') }}">
        @csrf
        @method('put')

        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Смена пароля</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="update_password">Смена пароля</a></li>
                    </ul>
                </div>
            </div>

            @if(session()->has('updatePassword'))
                <div class="alert alert-danger">
                    {{ session()->get('updatePassword') }}
                </div>
            @endif

            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title"> Текущий пароль</label>
                        <input type="password" name="old_password" class="input-regular" placeholder="Введите пароль"
                               data-required-text="Пожалуйста, введите свой пароль" data-validate="password" required>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Новый пароль</label>
                        <input type="password" name="new_password" class="input-regular" placeholder="Введите новый пароль"
                               data-required-text="Пожалуйста, введите новый пароль" data-validate="password" required>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Подтвердите Пароль</label>
                        <input type="password" name="new_password_confirmation" class="input-regular" placeholder="Подтвердите новый пароль"
                               data-required-text="Пожалуйста, подтвердите новый пароль" data-validate="password_confirmation" required>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
        </div>
    </form>
</div>

@endsection
@section('scripts')
    <script>
        window.addEventListener('load', () => {
            let inputs = document.querySelectorAll('input[required]');

            for(let i = 0; i < inputs.length; i++) {
                inputs[i].addEventListener('invalid', () => {
                    let msg = inputs[i].getAttribute('data-required-text');
                    inputs[i].setCustomValidity(msg);
                })
                inputs[i].addEventListener('input', () => {
                    inputs[i].setCustomValidity('');
                })
            }
        });
    </script>

@endsection
