@extends('admin.layouts.index')

@section('title', 'Профиль')

@section('content')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>Профиль</span></li>
        <li><span>{{$user->name}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$user->id}}</div>
            <h1 class="fund-header__title">{{$user->name}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Дата создания</div>
                <div class="property__text">{{$user->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Дата изменения</div>
                <div class="property__text">{{$user->updated_at}}</div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основная информация">Основная информация</a></li>
                    </ul>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    @if($errors->hasBag('updateProfile'))
                        <div class="alert alert-danger">
                            @foreach($errors->login as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

                    <div class="input-group">
                        <label class="input-group__title"> ФИО</label>
                        <input type="text" name="name" value="{{$user->name}}" placeholder="ФИО" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Почта</label>
                        <input type="email" name="email" value="{{$user->email}}" placeholder="Почта" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Роль</label>
                        <input type="text" name="role" value="{{$user->roles[0]->name}}" placeholder="Роль" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <a class="btn btn-primary" href="{{ route('panel.profile.edit') }}" title="Редактировать">Редактировать</a>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
