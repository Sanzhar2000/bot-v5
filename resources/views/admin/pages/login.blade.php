<!DOCTYPE html>
<html lang="ru">
<head>
    @include('admin.layouts.partials.head')
</head>
<body>

<input id="lang" type="hidden" value="ru">

<div class="authorization-wrapper">
    <div class="authorization-inner">
        <form method="POST" action="{{ route('general.login') }}">
            @csrf
            <img src="/admin/img/logo_light.png" alt="FlowDoc" class="logo">
            <div class="input-group">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                <input  id="email" type="email" name="email" placeholder="Email" class="input-regular" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback " role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="input-group">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                <input id="password" type="password" placeholder="Password" class="input-regular @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="text-right"><a href="#" title="Забыли пароль?" class="grey-link small">Забыли пароль?</a></div>
            </div>
            <div class="input-group">
                <button class="btn" style="width: 100%;">Войти</button>
            </div>
        </form>
    </div>
    <div class="copyright">Canvas Technologies, 2011-2020</div>
</div>

@include('admin.layouts.partials.scripts')

@yield('scripts')

<div id="message" class="modal" style="display: none;">
    <h4 class="title-secondary">Удаление</h4>
    <div class="plain-text">
        При удалении все данные будут удалены
    </div>
    <hr>
    <div class="buttons justify-end">
        <div><button class="btn btn--red">Удалить</button></div>
        <div><button class="btn" data-fancybox-close>Отмена</button></div>
    </div>
</div>

</body>
</html>
