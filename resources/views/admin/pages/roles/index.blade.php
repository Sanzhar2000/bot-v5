@extends('admin.layouts.index')

@section('title', 'Роли')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-4">
                <h1 class="title-primary" style="margin-bottom: 0">Управление ролями</h1>
            </div>
            <div class="col-md-8 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
                        @can('role-create')
                            <a class="btn btn-success" href="{{ route('panel.roles.create') }}">Создать новую роль</a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <br>
    <div class="block">
        <h2 class="title-secondary">Список ролей</h2>
        <table class="table table-records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 55%;">
                <col span="1" style="width: 40%;">
            </colgroup>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Название</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $key => $role)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $role->name }}</td>
                        <td>
                            <a class="btn btn-info" href="{{ route('panel.roles.show', [ 'lang' => \request()->segment(1), $role->id ]) }}">Смотреть</a>
                            @can('role-edit')
                                <a class="btn btn-primary" href="{{ route('panel.roles.edit', [ 'lang' => \request()->segment(1), $role->id]) }}">Редактировать</a>
                            @endcan
{{--                            @can('role-delete')--}}
{{--                                <a href="javascript:;" title="Удалить"--}}
{{--                                   onclick="document.querySelector('#model-{{ $role->id }}').submit()"--}}
{{--                                   class="icon-btn icon-btn--pink icon-delete"></a>--}}
{{--                                <form action="{{ route('panel.roles.destroy',  $role->id) }}" id="model-{{ $role->id }}" method="post">--}}
{{--                                    @csrf--}}
{{--                                    @method('DELETE')--}}
{{--                                </form>--}}
{{--                            @endcan--}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $roles->appends(\Illuminate\Support\Facades\Request::except('page'))->links("vendor.pagination.admin") }}
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
