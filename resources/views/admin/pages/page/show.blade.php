@extends('admin.layouts.index')

@section('title', 'Страницы')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.pages.index') }}">Управление страницами</a></li>
                    <li><span>{{ $page->title }}</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.pages.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="block">
        <h2 class="title-primary">Страница</h2>
        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Рус</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Рус">Рус</a></li>
                        <li><a href="javascript:;" id="" title="Қаз">Қаз</a></li>
                        <li><a href="javascript:;" id="" title="Қаз">Eng</a></li>
                    </ul>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title">Название</label>
                        <input type="text" name="title_ru" placeholder="Название" class="input-regular" value="{{ $page->title_ru }}" disabled>
                    </div>
                    <div class="input-group">
                        <div class="input-group__title">Описание</div>
                        <textarea name="description_ru" class="tinymce-text-here input-regular" disabled>{!! $page->description_ru !!}</textarea>
                    </div>
                </div>
                <div>
                    <div class="input-group">
                        <label class="input-group__title">Атауы</label>
                        <input type="text" name="title_kk" placeholder="Название" class="input-regular" value="{{ $page->title_kk }}" disabled>
                    </div>
                    <div class="input-group">
                        <div class="input-group__title">Сипаттамасы</div>
                        <textarea name="description_kk" class="tinymce-text-here input-regular" disabled>{!! $page->description_kk !!}</textarea>
                    </div>
                </div>
                <div>
                    <div class="input-group">
                        <label class="input-group__title">Title</label>
                        <input type="text" name="title_en" placeholder="Название" class="input-regular" value="{{ $page->title_en }}" disabled>
                    </div>
                    <div class="input-group">
                        <div class="input-group__title">Description</div>
                        <textarea name="description_en" class="tinymce-text-here input-regular" disabled>{!! $page->description_en !!}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                @can('page-edit')
                    <a class="btn btn-primary" href="{{ route('panel.pages.edit', $page->id) }}" title="Редактировать">Редактировать</a>
                @endcan
{{--                @can('page-delete')--}}
{{--                    <a class="btn btn-danger btn--red" href="{{ route('panel.pages.destroy', $page->id) }}"--}}
{{--                       onclick="event.preventDefault();--}}
{{--                                                 document.getElementById('delete-form').submit();" title="Удалить">Удалить--}}
{{--                    </a>--}}
{{--                    <form id="delete-form" action="{{ route('panel.pages.destroy', $page->id) }}" method="POST" class="d-none">--}}
{{--                        @csrf--}}
{{--                        @method('DELETE')--}}
{{--                    </form>--}}
{{--                @endcan--}}
            </div>
        </div>
    </div>

    <form class="block" method="post" action="{{ route('panel.pages.worker.add', $page->id) }}">
        @csrf
        @method('post')

        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8">
                <div class="input-group">
                    <label class="input-group__title">Добавить сотрудника</label>
                    <select name="worker_ids[]" class="chosen no-search input-regular" data-placeholder="Выберите сотрудника" required multiple>
                        <option value="" disabled>Выберите сотрудника</option>
                        @forelse($allWorkers as $worker)
                            <option value="{{ $worker->id }}">{{ $worker->name_ru }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <br>
                <button type="submit" class="btn btn--green">Добавить</button>
            </div>
        </div>
    </form>

    <div class="block">
        <h2 class="title-secondary">Список сотрудников</h2>
        <table class="table table-records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 22%;">
                <col span="1" style="width: 22%;">
                <col span="1" style="width: 25%;">
            </colgroup>
            <thead>
            <tr>
                <th>No</th>
                <th>ФИО</th>
                <th>Должность</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($workers as $worker)
                <tr>
                    <td>{{ $worker->id }}</td>
                    <td>{{ $worker->name_ru }}</td>
                    <td>{{ $worker->position_ru }}</td>
                    <td>
                        <a class="icon-btn icon-btn--green icon-eye" href="{{ route('panel.workers.show', $worker->id) }}" title="Смотреть"></a>
                        @can('worker-edit')
                            <a class="icon-btn icon-btn--yellow icon-edit" href="{{ route('panel.workers.edit', $worker->id) }}" title="Редактировать"></a>
                            <a href="javascript:;" title="Удалить"
                               onclick="document.querySelector('#model-{{ $worker->id }}').submit()"
                               class="btn btn-danger btn--red">Удалить</a>
                            <form action="{{ route('panel.pages.worker.delete', [$page->id, $worker->id]) }}" id="model-{{ $worker->id }}" method="post">
                                @csrf
                            </form>
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $workers->appends(\Illuminate\Support\Facades\Request::except('page'))->links("vendor.pagination.admin") }}
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
