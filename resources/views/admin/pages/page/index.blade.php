@extends('admin.layouts.index')

@section('title', 'Страницы')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <h1 class="title-primary" style="margin-bottom: 0">Управление страницами</h1>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
{{--                        @can('page-create')--}}
{{--                            <a class="btn btn-success" href="{{ route('panel.articles.create') }}">Добавить новость</a>--}}
{{--                        @endcan--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <br>
    <div class="block">
        <h2 class="title-secondary">Список страниц</h2>
        <table class="table table-records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 22%;">
                <col span="1" style="width: 22%;">
                <col span="1" style="width: 22%;">
                <col span="1" style="width: 25%;">
            </colgroup>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Название (РУС)</th>
                    <th>Название (ҚАЗ)</th>
                    <th>Название (ENG)</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pages as $page)
                    <tr>
                        <td>{{ $page->id }}</td>
                        <td>{{ $page->title_ru }}</td>
                        <td>{{ $page->title_kk }}</td>
                        <td>{{ $page->title_en }}</td>
                        <td>
                            <a class="icon-btn icon-btn--green icon-eye" href="{{ route('panel.pages.show', $page->id) }}" title="Смотреть"></a>
                            @can('page-edit')
                                <a class="icon-btn icon-btn--yellow icon-edit" href="{{ route('panel.pages.edit', $page->id) }}" title="Редактировать"></a>
                            @endcan
{{--                            @can('page-delete')--}}
{{--                                <a href="javascript:;" title="Удалить"--}}
{{--                                   onclick="document.querySelector('#model-{{ $page->id }}').submit()"--}}
{{--                                   class="icon-btn icon-btn--pink icon-delete"></a>--}}
{{--                                <form action="{{ route('panel.pages.destroy',  $page->id) }}" id="model-{{ $page->id }}" method="post">--}}
{{--                                    @csrf--}}
{{--                                    @method('DELETE')--}}
{{--                                </form>--}}
{{--                            @endcan--}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $pages->appends(\Illuminate\Support\Facades\Request::except('page'))->links("vendor.pagination.admin") }}
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
