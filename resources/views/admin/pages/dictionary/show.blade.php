@extends('admin.layouts.index')

@section('title', 'Справочник')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><span>Справочник</span></li>
                    <li><span>{{ $dictionary->name }}</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.dictionary-items.create', $dictionary) }}">Добавить</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="block">
        <h2 class="title-primary">Словарь</h2>
        <div class="input-group">
            <label class="input-group__title">Название (РУС)</label>
            <input type="text" name="name_ru" placeholder="Название" class="input-regular" value="{{ $dictionary->name_ru }}" disabled>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title">Название (ҚАЗ)</label>
            <input type="text" name="name_kk" placeholder="Название" class="input-regular" value="{{ $dictionary->name_kk }}" disabled>
        </div>
        <br>
        <div class="input-group">
            <label class="input-group__title">Название (ENG)</label>
            <input type="text" name="name_en" placeholder="Название" class="input-regular" value="{{ $dictionary->name_en }}" disabled>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                @can('dictionary-edit')
                    <a class="btn btn-primary" href="{{ route('panel.dictionaries.edit', $dictionary) }}">Редактировать</a>
                @endcan
{{--                @can('dictionary-delete')--}}
{{--                    <a class="btn btn-danger btn--red" href="{{ route('panel.dictionaries.destroy', $dictionary) }}"--}}
{{--                       onclick="event.preventDefault();--}}
{{--                                                 document.getElementById('delete-form').submit();" title="Удалить">Удалить--}}
{{--                    </a>--}}
{{--                    <form id="delete-form" action="{{ route('panel.dictionaries.destroy', $dictionary) }}" method="POST" class="d-none">--}}
{{--                        @csrf--}}
{{--                        @method('DELETE')--}}
{{--                    </form>--}}
{{--                @endcan--}}
            </div>
        </div>
    </div>

    <div class="block">
        <h2 class="title-secondary">Список</h2>
        <table class="table table-records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 22%;">
                <col span="1" style="width: 22%;">
                <col span="1" style="width: 22%;">
                <col span="1" style="width: 25%;">
            </colgroup>
            <thead>
            <tr>
                <th>No</th>
                <th>Название (Рус)</th>
                <th>Название (Қаз)</th>
                <th>Название (Eng)</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($items as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name_ru }}</td>
                    <td>{{ $item->name_kk }}</td>
                    <td>{{ $item->name_en }}</td>
                    <td>
                        <a class="icon-btn icon-btn--green icon-eye" href="{{ route('panel.dictionary-items.show', [$dictionary->id, $item->id]) }}" title="Смотреть"></a>
                        @can('dictionary-item-edit')
                            <a class="icon-btn icon-btn--yellow icon-edit" href="{{ route('panel.dictionary-items.edit', [$dictionary->id, $item->id]) }}" title="Редактировать"></a>
                        @endcan
                        @can('dictionary-item-delete')
                            <a href="javascript:;" title="Удалить"
                               onclick="document.querySelector('#model-{{ $item->id }}').submit()"
                               class="icon-btn icon-btn--pink icon-delete"></a>
                            <form action="{{ route('panel.dictionary-items.destroy',  [$dictionary->id, $item->id ]) }}" id="model-{{ $item->id }}" method="post">
                                @csrf
                                @method('DELETE')
                            </form>
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $items->appends(\Illuminate\Support\Facades\Request::except('page'))->links("vendor.pagination.admin") }}
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
