@extends('admin.layouts.index')

@section('title', 'Справочник')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><span>Справочник</span></li>
                    <li><a href="{{ route('panel.dictionaries.show', $dictionary) }}">{{ $dictionary->name }}</a></li>
                    <li><span>Редактировать</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.dictionaries.show', $dictionary) }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block">
        <form method="post" action="{{route('panel.dictionaries.update', $dictionary) }}">
            @csrf
            @method('put')
            <h2 class="title-primary">Словарь</h2>
            <div class="input-group">
                <label class="input-group__title">Название (РУС)<span class="required">*</span></label>
                <input type="text" name="name_ru" placeholder="Название" class="input-regular" value="{{ $dictionary->name_ru }}" required>
            </div>
            <br>
            <div class="input-group">
                <label class="input-group__title">Название (ҚАЗ)<span class="required">*</span></label>
                <input type="text" name="name_kk" placeholder="Название" class="input-regular" value="{{ $dictionary->name_kk }}" required>
            </div>
            <br>
            <div class="input-group">
                <label class="input-group__title">Название (ENG)<span class="required">*</span></label>
                <input type="text" name="name_en" placeholder="Название" class="input-regular" value="{{ $dictionary->name_en }}" required>
            </div>
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
{{--            @can('dictionary-delete')--}}
{{--                <a class="icon-btn icon-btn--pink icon-delete" href="{{ route('panel.dictionary-items.destroy', [$dictionary->id, $item->id ]) }}"--}}
{{--                   onclick="event.preventDefault();--}}
{{--                                                         document.getElementById('delete-form').submit();">--}}
{{--                </a>--}}
{{--                <form id="delete-form" action="{{ route('panel.dictionary-items.destroy', [$dictionary->id, $item->id ]) }}" method="POST" class="d-none">--}}
{{--                    @csrf--}}
{{--                    @method('DELETE')--}}
{{--                </form>--}}
{{--            @endcan--}}
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
