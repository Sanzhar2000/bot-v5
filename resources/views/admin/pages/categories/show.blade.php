@extends('admin.layouts.index')

@section('title', 'Новости')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.articles.index') }}">Управление новостями</a></li>
                    <li><span>{{ $article->title }}</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.articles.index') }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="block">
        <h2 class="title-primary">Новость</h2>
        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Рус</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Рус">Рус</a></li>
                        <li><a href="javascript:;" id="" title="Қаз">Қаз</a></li>
                        <li><a href="javascript:;" id="" title="Қаз">Eng</a></li>
                    </ul>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title">Название</label>
                        <input type="text" name="title_ru" placeholder="Название"
                               class="input-regular" value="{{ $article->title_ru }}" disabled>
                    </div>
                    <div class="input-group">
                        <div class="input-group__title">Описание</div>
                        <textarea name="description_ru" class="tinymce-text-here input-regular"
                                  disabled>{!! $article->description_ru !!}</textarea>
                    </div>
                </div>
                <div>
                    <div class="input-group">
                        <label class="input-group__title">Атауы</label>
                        <input type="text" name="title_kk" placeholder="Название"
                               class="input-regular" value="{{ $article->title_kk }}" disabled>
                    </div>
                    <div class="input-group">
                        <div class="input-group__title">Сипаттамасы</div>
                        <textarea name="description_kk" class="tinymce-text-here input-regular"
                                  disabled>{!! $article->description_kk !!}</textarea>
                    </div>
                </div>
                <div>
                    <div class="input-group">
                        <label class="input-group__title">Title</label>
                        <input type="text" name="title_en" placeholder="Название"
                               class="input-regular" value="{{ $article->title_en }}" disabled>
                    </div>
                    <div class="input-group">
                        <div class="input-group__title">Description</div>
                        <textarea name="description_en" class="tinymce-text-here input-regular"
                                  disabled>{!! $article->description_en !!}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="input-group">
            <div class="input-group__title">Фотограция</div>
            <a href="{{ $article->image }}" target="_blank">
                <img class="article-image" src="{{ $article->image ?? ''}}" height="200" length="400">
            </a>
        </div>
        <div class="input-group">
            <label class="input-group__title">Опубликован</label>
            <input type="text" name="is_published" title="Опубликован" class="input-regular"
                   value="{{ $article->is_published == '1' ? 'Да' : 'Нет' }}" disabled>
        </div>
        <div class="input-group">
            <label class="input-group__title">Дата публикации</label>
            <input type="text" name="published_at" title="Опубликован" class="input-regular"
                   value="{{ $article->published_at ?? '-' }}" disabled>
        </div>
        <div class="input-group">
            <label class="input-group__title">Для кого</label>
            <input type="text" name="type" title="Для кого" class="input-regular" disabled
                   value="@if($article->type == '1') Общедоступный @elseif($article->type == '2') Авторизованным @elseif($article->type == '3') Для членов МСК @endif">
        </div>
        <div class="input-group">
            <label class="input-group__title">Категория</label>
            <input type="text" name="category" title="Категория" class="input-regular" value="{{ $article->category->name }}" disabled>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                @can('article-edit')
                    <a class="btn btn-primary" href="{{ route('panel.articles.edit', $article->id) }}">Редактировать</a>
                @endcan
                @can('article-delete')
                    <a class="btn btn-danger btn--red" href="{{ route('panel.articles.destroy', $article->id) }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();" title="Удалить">Удалить
                    </a>
                    <form id="delete-form" action="{{ route('panel.articles.destroy', $article->id) }}" method="POST" class="d-none">
                        @csrf
                        @method('DELETE')
                    </form>
                @endcan
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
