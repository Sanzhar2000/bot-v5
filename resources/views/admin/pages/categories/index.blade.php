@extends('admin.layouts.index')

@section('title', 'Новости')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <h1 class="title-primary" style="margin-bottom: 0">Управление категориями</h1>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
                        @can('article-create')
                            <a class="btn btn-success" href="{{ route('panel.articles.create') }}">Добавить новость</a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <br>
    <div class="block">
        <h2 class="title-secondary">Список новостей</h2>
        <table class="table table-records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 20%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 25%;">
            </colgroup>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Название</th>
                    <th>Опубликован</th>
                    <th>Дата публикации</th>
                    <th>Категория</th>
                    <th>Для кого</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($articles as $article)
                    <tr>
                        <td>{{ $article->id }}</td>
                        <td>{{ $article->title }}</td>
                        <td>{{ $article->is_published == '1' ? 'Да' : 'Нет'}}</td>
                        <td>{{ $article->published_at ?? '-' }}</td>
                        <td>{{ $article->category->name }}</td>
                        <td>
                            @if($article->type == '1')
                                Общедоступный
                            @elseif($article->type == '2')
                                Авторизованным
                            @elseif($article->type == '3')
                                Для членов МСК
                            @endif
                        </td>
                        <td>
                            <a class="icon-btn icon-btn--green icon-eye" href="{{ route('panel.articles.show', $article->id) }}" title="Смотреть"></a>
                            @can('article-edit')
                                <a class="icon-btn icon-btn--yellow icon-edit" href="{{ route('panel.articles.edit', $article->id) }}" title="Редактировать"></a>
                            @endcan
                            @can('article-delete')
                                <a href="javascript:;" title="Удалить"
                                   onclick="document.querySelector('#model-{{ $article->id }}').submit()"
                                   class="icon-btn icon-btn--pink icon-delete"></a>
                                <form action="{{ route('panel.articles.destroy',  $article->id) }}" id="model-{{ $article->id }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $articles->appends(\Illuminate\Support\Facades\Request::except('page'))->links("vendor.pagination.admin") }}
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
    <!---->
@endsection
