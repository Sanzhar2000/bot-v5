@extends('admin.layouts.index')

@section('title', 'Новости')

@section('content')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-8">
                <ul class="breadcrumbs">
                    <li><a href="{{ route('panel.articles.index') }}">Управление новостями</a></li>
                    <li><span>Добавить новость</span></li>
                </ul>
            </div>
            <div class="col-md-4 text-right-md text-right-lg">
                <div class="flex-form">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('panel.articles.index', [ 'lang' => \request()->segment(1) ]) }}">Назад</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block">
        @can('article-create')
        <form method="post" action="{{route('panel.articles.store') }}" enctype="multipart/form-data">
            @csrf
            <h2 class="title-primary">Новость</h2>
            <div class="tabs">
                <div class="mobile-dropdown">
                    <div class="mobile-dropdown__title dynamic">Рус</div>
                    <div class="mobile-dropdown__desc">
                        <ul class="tabs-titles">
                            <li class="active"><a href="javascript:;" title="Рус">Рус</a></li>
                            <li><a href="javascript:;" id="" title="Қаз">Қаз</a></li>
                            <li><a href="javascript:;" id="" title="Қаз">Eng</a></li>
                        </ul>
                    </div>
                </div>
                <div class="tabs-contents">
                    <div class="active">
                        <div class="input-group">
                            <label class="input-group__title">Название <span class="required">*</span></label>
                            <input type="text" name="title_ru" placeholder="Название" class="input-regular">
                        </div>
                        <br>
                        <div class="input-group">
                            <div class="input-group__title">Описание <span class="required">*</span></div>
                            <textarea name="description_ru" class="tinymce-text-here input-regular"></textarea>
                        </div>
                    </div>
                    <div>
                        <div class="input-group">
                            <label class="input-group__title">Атауы <span class="required">*</span></label>
                            <input type="text" name="title_kk" placeholder="Название" class="input-regular">
                        </div>
                        <br>
                        <div class="input-group">
                            <div class="input-group__title">Сипаттамасы <span class="required">*</span></div>
                            <textarea name="description_kk" class="tinymce-text-here input-regular"></textarea>
                        </div>
                    </div>
                    <div>
                        <div class="input-group">
                            <label class="input-group__title">Title <span class="required">*</span></label>
                            <input type="text" name="title_en" placeholder="Название" class="input-regular">
                        </div>
                        <br>
                        <div class="input-group">
                            <div class="input-group__title">Description <span class="required">*</span></div>
                            <textarea name="description_en" class="tinymce-text-here input-regular" ></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group">
                        <label class="checkbox">
                            <input type="checkbox" name="is_published" value="1">
                            <span>Опубликовано</span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="input-group">
                        <label class="input-group__title">Дата публикации</label>
                        <label class="time">
                            <input type="text" name="published_at" value="" placeholder=""
                                   class="input-regular custom-datepicker">
                        </label>
                    </div>
                </div>
            </div>
            <div class="input-group">
                <label class="input-group__title">Для кого</label>
                <select name="type" class="input-regular chosen" data-placeholder="">
                    <option value="1">Общедоступный</option>
                    <option value="2">Авторизованным</option>
                    <option value="3">Для членов МСК</option>
                </select>
            </div>
            <div class="input-group">
                <label class="input-group__title">Категория</label>
                <select name="category_id" class="input-regular chosen" data-placeholder="" required>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label class="input-group__title">Фото</label>
                    <input type="file" name="image" class="form-control" placeholder="image">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
        @endcan
    </div>
</div>

@endsection
@section('scripts')
    <!--Only this page's scripts-->
        <script src="https://cdn.tiny.cloud/1/eutvjr9zyhc4qtchwyfwhutknw2iunsf80kiuye2fdomu2wd/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script src="/admin/js/api.js"></script>
        <script src="/admin/js/tinymce.js"></script>
    <!---->
@endsection
