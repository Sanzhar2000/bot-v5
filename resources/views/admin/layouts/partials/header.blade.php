<header class="header">
    <div class="container container-fluid">
        <a href="javascript:;" title="Свернуть/развернуть навигацию" class="menu-btn icon-menu"></a>
        <a href="/" title="Главная" class="logo hidden-md hidden-lg"><img src="/admin/img/logo_light.svg" alt=""></a>
        <div class="language hidden-sm hidden-xs">
{{--            <a href="{{ route('switchLang', [$lang, 'ru']) }}" title="РУС" class="{{ $lang == "ru" ? 'active' : '' }}">РУС</a>--}}
{{--            <a href="{{ route('switchLang', [$lang, 'en']) }}" title="ENG" class="{{ $lang == "en" ? 'active' : '' }}">ENG</a>--}}
{{--            <a href="{{ route('switchLang', [$lang, 'kk']) }}" title="Қаз" class="{{ $lang == "kk" ? 'active' : '' }}">Қаз</a>--}}
        </div>
        <div class="header-dropdown account-nav">
            <div class="header-dropdown__title">
{{--                <span> @lang('admin/partials.header.welcome'), Илья!</span> <img src="/admin/img/user.svg" alt=""> <i--}}
                <span> Добро пожаловать, {{ auth()->user()->name }}</span> <img src="/admin/img/user.svg" alt=""> <i
                    class="icon-chevron-down"></i>
            </div>
            <div class="header-dropdown__desc">
                <ul>
{{--                    <li class="language hidden-md hidden-lg">--}}
{{--                        <a href="{{ route('switchLang', [$lang, 'ru']) }}" title="РУС" class="active">РУС</a>--}}
{{--                        <a href="{{ route('switchLang', [$lang, 'en']) }}" title="ENG">ENG</a>--}}
{{--                        <a href="{{ route('switchLang', [$lang, 'kk']) }}" title="Қаз">Қаз</a>--}}
{{--                    </li>--}}
                    <li>
                        <a class="dropdown-item" href="{{ route('general.logout', $lang) }}"
                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                            Выйти
                        </a>

                        <form id="logout-form" action="{{ route('general.logout', $lang) }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
