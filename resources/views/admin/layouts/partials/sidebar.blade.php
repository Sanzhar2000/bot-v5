<aside class="sidebar">
    <div class="sidebar__top hidden-sm hidden-xs">
        <a href="/" title="Главная" class="logo"><img src="/admin/img/logo_light_text.png" alt=""></a>
    </div>
    <div class="menu-wrapper">
        <ul class="menu">
            <li class="dropdown">
                <a href="javascript:;" title="Профиль"><i class="icon-directory"></i>Профиль</a>
                <ul>
                    <li><a href="{{ route('panel.profile.show') }}" title="Смотреть">Смотреть</a></li>
                    <li><a href="{{ route('panel.profile.edit') }}" title="Редактировать">Редактировать</a></li>
                </ul>
            </li>
{{--            <li class="dropdown">--}}
{{--                <a href="javascript:;" title="@lang('admin/partials.sidebar.roles')"><i class="icon-users"></i>Роли</a>--}}
{{--                <ul>--}}
{{--                    <li><a class="nav-link" href="{{ route('panel.roles.index') }}">Список ролей</a></li>--}}
{{--                    <li><a href="{{ route('panel.roles.create') }}" title="Добавить" class="add">+Добавить</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
            <li class="dropdown">
                <a href="javascript:;" title="@lang('admin/partials.sidebar.articles')"><i class="icon-users"></i>Новости</a>
                <ul>
                    <li><a class="nav-link" href="{{ route('panel.articles.index') }}">Список новостей</a></li>
                    <li><a href="{{ route('panel.articles.create') }}" title="Добавить" class="add">+Добавить</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Страницы"><i class="icon-users"></i>Пользователи</a>
                <ul>
                    <li><a class="nav-link" href="{{ route('panel.users.index', ['type' => 'all']) }}">Все пользователи</a></li>
                    <li><a class="nav-link" href="{{ route('panel.users.index', ['type' => 'registered']) }}">Зарегистрированные пользователи</a></li>
                    <li><a class="nav-link" href="{{ route('panel.users.index', ['type' => 'members']) }}">Члены МСК</a></li>
                    <li><a href="{{ route('panel.users.create') }}" title="Добавить" class="add">+Добавить пользователя</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Страницы"><i class="icon-users"></i>Страницы</a>
                <ul>
                    <li><a class="nav-link" href="{{ route('panel.pages.index') }}">Список страниц</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Страницы"><i class="icon-users"></i>Сотрудники</a>
                <ul>
                    <li><a class="nav-link" href="{{ route('panel.workers.index') }}">Список сотрудников</a></li>
                    <li><a href="{{ route('panel.workers.create') }}" title="Добавить" class="add">+Добавить</a></li>
                </ul>
            </li>
{{--            <li class="dropdown">--}}
{{--                <a href="javascript:;" title="Страницы"><i class="icon-users"></i>Заявки</a>--}}
{{--                <ul>--}}
{{--                    <li><a class="nav-link" href="{{ route('panel.orders.index') }}">Список заявок</a></li>--}}
{{--                    <li><a href="{{ route('panel.orders.create') }}" title="Добавить" class="add">+Добавить</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
            <li class="dropdown">
                <a href="javascript:;" title="Страницы"><i class="icon-users"></i>Заявки</a>
                <ul>
{{--                    <li><a class="nav-link" href="{{ route('panel.orders.index') }}">Список заявок</a></li>--}}
                    <li><a class="nav-link" href="{{ route('panel.applications.index') }}">Зявки на вступление в члены МСК</a></li>
{{--                    <li><a href="{{ route('panel.applications.create') }}" title="Добавить" class="add">+Добавить</a></li>--}}
                </ul>
            </li>
{{--            <li class="dropdown">--}}
{{--                <a href="javascript:;" title="Справочники"><i class="icon-users"></i>Справочники</a>--}}
{{--                <ul>--}}
{{--                    @foreach($dictionaries as $dictionary)--}}
{{--                        <li class="dropdown">--}}
{{--                            <a href="javascript:;" title="{{ $dictionary->name }}"><i class="icon-users"></i>{{ $dictionary->name }}</a>--}}
{{--                            <ul>--}}
{{--                                <li><a class="nav-link" href="{{ route('panel.dictionaries.show', $dictionary) }}">Список</a></li>--}}
{{--                                <li><a href="{{ route('panel.dictionary-items.create', $dictionary) }}" title="Добавить" class="add">+Добавить</a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                    @endforeach--}}
{{--                </ul>--}}
{{--            </li>--}}
        </ul>
    </div>
</aside>
