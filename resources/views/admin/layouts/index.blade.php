<!DOCTYPE html>
<html lang="ru">
<head>
    @include('admin.layouts.partials.head')

    <script>
        window.Laravel = {
            "csrfToken": "{{ csrf_token() }}",
            "lang": "{{ \Illuminate\Support\Facades\App::getLocale() }}",
            "baseUrl": "{{ env('APP_URL') }}"
        }
    </script>
</head>
<body>

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
    @include('admin.layouts.partials.sidebar')

    <div class="right-wrapper">
        @include('admin.layouts.partials.header')

        <main class="main">

            @yield('content')

        </main>

        @include('admin.layouts.partials.footer')

    </div>
</div>

@include('admin.layouts.partials.scripts')

@yield('scripts')

</body>
</html>
