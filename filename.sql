-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: localhost    Database: bot-v5
-- ------------------------------------------------------
-- Server version	8.0.25-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_kk` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Category_1','Категория_:1','Қатегөрия_1','2021-08-26 06:08:34','2021-08-26 06:08:34'),(2,'Category_2','Категория_:2','Қатегөрия_2','2021-08-26 06:08:34','2021-08-26 06:08:34'),(3,'Category_3','Категория_:3','Қатегөрия_3','2021-08-26 06:08:34','2021-08-26 06:08:34'),(4,'Category_4','Категория_:4','Қатегөрия_4','2021-08-26 06:08:34','2021-08-26 06:08:34');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2021_08_24_091808_create_categories_table',1),(5,'2021_08_24_091819_create_sub_categories_table',1),(6,'2021_08_24_103242_create_questions_table',1),(7,'2021_08_25_082954_create_states_table',1),(8,'2021_08_26_060254_create_permission_tables',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` bigint unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_roles` (
  `role_id` bigint unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `questions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `sub_category_id` bigint unsigned NOT NULL,
  `title_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_kk` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer_kk` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questions_sub_category_id_foreign` (`sub_category_id`),
  CONSTRAINT `questions_sub_category_id_foreign` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,1,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:34','2021-08-26 06:08:34'),(2,1,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:34','2021-08-26 06:08:34'),(3,1,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:34','2021-08-26 06:08:34'),(4,1,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:34','2021-08-26 06:08:34'),(5,1,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:34','2021-08-26 06:08:34'),(6,2,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:34','2021-08-26 06:08:34'),(7,2,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(8,2,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(9,2,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(10,2,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(11,3,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(12,3,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(13,3,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(14,3,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(15,3,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(16,4,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(17,4,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(18,4,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(19,4,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(20,4,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(21,5,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(22,5,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(23,5,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(24,5,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(25,5,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(26,6,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(27,6,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(28,6,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(29,6,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(30,6,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(31,7,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(32,7,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(33,7,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(34,7,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(35,7,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(36,8,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(37,8,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(38,8,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(39,8,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(40,8,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(41,9,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(42,9,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(43,9,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(44,9,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(45,9,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(46,10,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(47,10,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(48,10,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(49,10,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(50,10,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(51,11,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(52,11,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(53,11,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(54,11,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(55,11,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(56,12,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(57,12,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(58,12,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(59,12,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(60,12,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(61,13,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(62,13,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(63,13,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(64,13,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(65,13,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(66,14,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(67,14,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(68,14,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(69,14,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(70,14,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(71,15,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(72,15,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(73,15,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(74,15,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(75,15,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35'),(76,16,'Question: 1','Вопрос: 1','Сұрақ: 1','Answer: 1','Ответ: 1','Жауап: 1','2021-08-26 06:08:35','2021-08-26 06:08:35'),(77,16,'Question: 2','Вопрос: 2','Сұрақ: 2','Answer: 2','Ответ: 2','Жауап: 2','2021-08-26 06:08:35','2021-08-26 06:08:35'),(78,16,'Question: 3','Вопрос: 3','Сұрақ: 3','Answer: 3','Ответ: 3','Жауап: 3','2021-08-26 06:08:35','2021-08-26 06:08:35'),(79,16,'Question: 4','Вопрос: 4','Сұрақ: 4','Answer: 4','Ответ: 4','Жауап: 4','2021-08-26 06:08:35','2021-08-26 06:08:35'),(80,16,'Question: 5','Вопрос: 5','Сұрақ: 5','Answer: 5','Ответ: 5','Жауап: 5','2021-08-26 06:08:35','2021-08-26 06:08:35');
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `states` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `chat_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `step` smallint DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `sub_category_id` int DEFAULT NULL,
  `question_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `states`
--

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sub_categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `category_id` bigint unsigned NOT NULL,
  `title_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_kk` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_categories_category_id_foreign` (`category_id`),
  CONSTRAINT `sub_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_categories`
--

LOCK TABLES `sub_categories` WRITE;
/*!40000 ALTER TABLE `sub_categories` DISABLE KEYS */;
INSERT INTO `sub_categories` VALUES (1,1,'SubCategory1 : 1','СубКатегория1 : 1','СубҚатегөрия1 : 1','2021-08-26 06:08:34','2021-08-26 06:08:34'),(2,1,'SubCategory1 : 2','СубКатегория1 : 2','СубҚатегөрия1 : 2','2021-08-26 06:08:34','2021-08-26 06:08:34'),(3,1,'SubCategory1 : 3','СубКатегория1 : 3','СубҚатегөрия1 : 3','2021-08-26 06:08:34','2021-08-26 06:08:34'),(4,1,'SubCategory1 : 4','СубКатегория1 : 4','СубҚатегөрия1 : 4','2021-08-26 06:08:34','2021-08-26 06:08:34'),(5,2,'SubCategory2 : 1','СубКатегория2 : 1','СубҚатегөрия2 : 1','2021-08-26 06:08:34','2021-08-26 06:08:34'),(6,2,'SubCategory2 : 2','СубКатегория2 : 2','СубҚатегөрия2 : 2','2021-08-26 06:08:34','2021-08-26 06:08:34'),(7,2,'SubCategory2 : 3','СубКатегория2 : 3','СубҚатегөрия2 : 3','2021-08-26 06:08:34','2021-08-26 06:08:34'),(8,2,'SubCategory2 : 4','СубКатегория2 : 4','СубҚатегөрия2 : 4','2021-08-26 06:08:34','2021-08-26 06:08:34'),(9,3,'SubCategory3 : 1','СубКатегория3 : 1','СубҚатегөрия3 : 1','2021-08-26 06:08:34','2021-08-26 06:08:34'),(10,3,'SubCategory3 : 2','СубКатегория3 : 2','СубҚатегөрия3 : 2','2021-08-26 06:08:34','2021-08-26 06:08:34'),(11,3,'SubCategory3 : 3','СубКатегория3 : 3','СубҚатегөрия3 : 3','2021-08-26 06:08:34','2021-08-26 06:08:34'),(12,3,'SubCategory3 : 4','СубКатегория3 : 4','СубҚатегөрия3 : 4','2021-08-26 06:08:34','2021-08-26 06:08:34'),(13,4,'SubCategory4 : 1','СубКатегория4 : 1','СубҚатегөрия4 : 1','2021-08-26 06:08:34','2021-08-26 06:08:34'),(14,4,'SubCategory4 : 2','СубКатегория4 : 2','СубҚатегөрия4 : 2','2021-08-26 06:08:34','2021-08-26 06:08:34'),(15,4,'SubCategory4 : 3','СубКатегория4 : 3','СубҚатегөрия4 : 3','2021-08-26 06:08:34','2021-08-26 06:08:34'),(16,4,'SubCategory4 : 4','СубКатегория4 : 4','СубҚатегөрия4 : 4','2021-08-26 06:08:34','2021-08-26 06:08:34');
/*!40000 ALTER TABLE `sub_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'User Admin','admin@gmail.com','2021-08-26 06:08:32','$2y$10$CyHlImpkOLf9U/i4HBQodudMRs5wUAKmZMrL.4ee4M0d/qHI5MKVm',NULL,'2021-08-26 06:08:32','2021-08-26 06:08:32'),(2,'User Manager','manager@gmail.com','2021-08-26 06:08:32','$2y$10$sXRcDPSQhlHmSo.4qQAL.eeR7OsfLBvAIZddJevgvrFMgpcpMadGa',NULL,'2021-08-26 06:08:32','2021-08-26 06:08:32'),(3,'Willis Gleason','braun.demetris@hotmail.com',NULL,'$2y$10$Fax6LcEApJ1MYySUkezU0u26offGpSgV/frrFYjOAj9pgHrZH2ccG',NULL,'2021-08-26 06:08:33','2021-08-26 06:08:33'),(4,'Dorian Ziemann','anna.purdy@dubuque.com',NULL,'$2y$10$lxv4kww/6G9pjSHZkkX4ouBj1Hl0akJNSpvn6QtGjoQ0IJmDEqaI.',NULL,'2021-08-26 06:08:33','2021-08-26 06:08:33'),(5,'Marcelino Lueilwitz I','geoffrey.hackett@gmail.com',NULL,'$2y$10$kOMa71WmcMtsxZPrTpmT8ORtZy9BbZDRNkNE/gNdWkUpppoC6NZnK',NULL,'2021-08-26 06:08:33','2021-08-26 06:08:33'),(6,'Mr. Ervin Prosacco IV','felipa08@gmail.com',NULL,'$2y$10$9hYq7UcNSsGyGYeFffateuQKZNRx/Tw915iV7C/Xm3rY1XRw8ehpq',NULL,'2021-08-26 06:08:33','2021-08-26 06:08:33'),(7,'Anabel Schulist','zlangosh@gmail.com',NULL,'$2y$10$qPVB3eTHZnPr4twoB3uPS.XCTpW.B74fiBVXSV4bqeYCAFFhmliQy',NULL,'2021-08-26 06:08:33','2021-08-26 06:08:33'),(8,'Augustine Langworth','jrussel@hotmail.com',NULL,'$2y$10$VMh5uXJs0Eiy7EwFlw51t.qe35tNgD0j/Wb1LUHzJLgmtd6azMz5O',NULL,'2021-08-26 06:08:34','2021-08-26 06:08:34'),(9,'Uriah Walsh','kkulas@will.com',NULL,'$2y$10$sc7Cm/FU9Ho8D5TSZI1x5uncnJ/JkZpJdgLpn9edA1Jsd0lw5PH/O',NULL,'2021-08-26 06:08:34','2021-08-26 06:08:34'),(10,'Nannie Romaguera','kiana.stroman@jacobi.org',NULL,'$2y$10$/VOhhZTWPykxkIEMTf1ey.eAYH0LuFLmqyrteUjnMMmFjsOi/mZoS',NULL,'2021-08-26 06:08:34','2021-08-26 06:08:34'),(11,'Mr. Adrain West','julius.homenick@hotmail.com',NULL,'$2y$10$SJyfce/D/csOU3XWlNe5suHLiotFTwJX0s1NEgM/hgCB.BrPUNLgG',NULL,'2021-08-26 06:08:34','2021-08-26 06:08:34'),(12,'Prof. Allie Ullrich V','wisozk.wilma@tremblay.net',NULL,'$2y$10$cFUBzBS2jvFYn3xRNlF3MOhkq32iH/U8Xt9nU80nlQsCuA.hMgbRy',NULL,'2021-08-26 06:08:34','2021-08-26 06:08:34'),(13,'User','user@gmail.com','2021-08-26 06:08:34','$2y$10$IJRo0.kn7CH4FuxDpQRjPeBC9l9BDVpXgdA0UpE/QxrAieQ0nAaqW',NULL,'2021-08-26 06:08:34','2021-08-26 06:08:34');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-26  6:08:47
